# Security in Azure
General area to cover various areas of security in Azure which are plentiful. When certain sections grow too large, lets consider splitting out to separate files and linking?

##### Contributing
Create a branch or fork and PR changes. Ping the group and chat if you have ideas to talk thru first if you would like!


## Table of Contents

* [KQL](#kql)
    * [KQL Training Resources](#kql-training-resources)
* [SENTINEL](#sentinel)
    * [Sentinel Training Resources](#sentinel-training-resources)

### KQL
Kusto Query Language is a powerful tool to explore your data and discover patterns, identify anomalies and outliers, create statistical modeling, and more. The query uses schema entities that are organized in a hierarchy similar to SQL's: databases, tables, and columns.

KQL is used on many Azure resources riding on Log Analytics workspaces

#### KQL Training Resources
- [Microsoft Log Analytics Demo](https://aka.ms/lademo)
With any microsoft account (work/personal) you can use the FREE log analytics demo to play with KQL and data; super easy way to have a playground for KQL

- [MS Docs Kusto Query Language (KQL) overview](https://docs.microsoft.com/en-us/azure/data-explorer/kusto/query/)

- [Marcus Bakker Cheatsheet PDF](https://github.com/marcusbakker/KQL)
get a ton of basic KQL working knowledge and tricks fast

- [TeachJing YouTube Video](https://www.youtube.com/watch?v=Qiv-6gH4gnA)
Walk-through of Bakker Cheatsheet

- [KnowOps/Dana Epp YouTube Video](https://www.youtube.com/watch?v=DuWBLsgqhaI)
Recon your Azure resources with Kusto Query Language - a couple practical examples and walks thru building queries

- [Must Learn KQL Book](https://github.com/rod-trent/MustLearnKQL)
Free (or paid to send a donation) KQL book from Rod Trent. The repo also has a ton more links for training resources

### SENTINEL
Microsoft Sentinel is a scalable, cloud-native, security information event management (SIEM) and security orchestration automated response (SOAR) solution. Azure Sentinel delivers intelligent security analytics and threat intelligence across the enterprise, providing a single solution for alert detection, threat visibility, proactive hunting, and threat response.

#### Sentinel Training Resources
- [Microsoft Ninja Series](https://aka.ms/AzureSentinelNinja)
- [TeachJings YouTube Sentinel Lab Series](https://www.youtube.com/playlist?list=PLM3TOIlrnaI6B9ikTWz8A0FY812ZqBO3_)

#### Sentinel things to follow
- [LinkedIn Microsoft Sentinel Community](https://www.linkedin.com/groups/8768381/)
- [Twitter hashtag for Sentinel](https://twitter.com/search?q=%23MicrosoftSentinel)
- [Twitter hashtag for MSFT Security](https://twitter.com/search?q=%23MicrosoftSecurity)
- [Twitter account Rod Trent from MSFT](https://twitter.com/rodtrent)
